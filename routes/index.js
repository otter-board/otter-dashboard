var express = require('express');
let testController = require('../controller/testController');  
let InterfacesController = require('../controller/InterfacesController');  
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/test', testController.testGraph);
router.get('/interfaces', InterfacesController.general);

module.exports = router;
