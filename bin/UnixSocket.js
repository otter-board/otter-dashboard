//Import net library, needed to use unix socket
var net = require('net');


class Unix{
    
    
    static socket;
    connected = true;
    /*  The constructor wait for the socket path
    **  and for the client object created to exhange
    **  with the client part of the application
    */
    constructor(socket, client){
       
       
        //Etablishment of connexion with the socket
  
        this.socket = net.createConnection(socket);
        
        //Define some debug message on some event*/
        this.socket.on("connect", () => {
            console.log("Connected to the server");
            this.connected=true;
            if (client.connected) {
                client.io.emit("reload")
                client.io.emit("Notification",["Connexion établie", "La connexion avec le module core à été atablie avec succès", "11:56"])
            }            
        })

        this.socket.on("error", () => {            
            if (client.connected) {
                if (this.connected) {
                    console.log("Error");
                    this.connected=false
                    client.io.emit("Notification",["Erreur de connexion", "L'application n'arrive pas a joinde le module 'core'", "11:56"])
                }
            }
        })
        this.socket.on("end", () => {console.log("Socket closed by the pair");})

        //When the connexion is close, the socket try to reconnect
        

        this.socket.on("close", ()=>{
            console.log("Connexion close");
            setTimeout(()=>{
                this.socket.connect(socket)}, 5000);                
        })
            
        //Send the received data to the client if it's connected
        
        this.socket.on("data", (data)=>{
            if (client.connected) {
                var dataobj = JSON.parse(data.toString('utf8'));
                  for(let key in dataobj){
                      console.log(key+":"+dataobj[key]);
                      client.io.to(client.current).emit(key,dataobj[key]);
                  }
            }
        })

    }

    //Object methode to write on the socket
    write(data){
        this.socket.write(data)
    }


}

module.exports = Unix;