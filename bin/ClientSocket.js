class Client{
    
    current=null;
    /*  The constructeur require the server used
    **  by express to emit and received throught it
    */
    constructor(server){
        
        
        this.socket= null;
        this.connected= false;
        
        //IO server creation
        this.io= require("socket.io")(server)

        /*  If connection is detected fill
        **  the socket variable with the component
        **  useful to exchange with the client
        */ 
        this.io.on('connect', (socket) =>{
            this.socket=socket;
            this.connected=true;
        })

        /*  If deconnection is detected destroy
        **  the socket variable
        */ 
        this.io.on('disconnect', (socket) =>{
            this.socket=null;
            this.connected=false;
        })
    }

    //Object function to retransmit request from client to core
    startTransmission(unix){
        this.io.sockets.on('connection',
            (socket) => {
                socket.on("loaded", (page_name)=>{
                    unix.write("GET_"+page_name)
                    console.log("sending GET_"+page_name);
                    this.current=socket.id;
                })
            }
        )
    }
}

module.exports = Client;