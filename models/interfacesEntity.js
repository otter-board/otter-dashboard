let unixsocket = require('../bin/UnixSocket')

unixsocket.write("GET_Interfaces");
let loaded = false
let rc_data
unixsocket.isDataSend((data) => {
    loaded=true;
    console.log(data);
    rc_data=data.toString('utf8');
})

module.exports ={
    getNbInterfaces: () => {return rc_data},
}
