socket.on("Notification", (notifContent)=>{
    $("#notification").prepend(
        '<a href="javascript:void(0)" class="list-group-item">\
            <div class="media">\
                <div class="media-left valign-middle"><i class="icon-monitor3 icon-bg-circle bg-red bg-darken-1"></i></div>\
                <div class="media-body">\
                    <h6 class="media-heading red darken-1">'+notifContent[0]+'</h6>\
                    <p class="notification-text font-small-3 text-muted">'+notifContent[1]+'</p><small>\
                    <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">'+notifContent[2]+'</time></small>\
                </div>\
            </div>\
            </a>'
    );
})